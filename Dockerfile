FROM debian:bullseye
MAINTAINER hans@eds.org

ARG  LANG=C.UTF-8 
ARG  DEBIAN_FRONTEND=noninteractive

ARG UID=113
ARG GID=118
RUN groupadd -g $GID --non-unique debian-transmission
RUN useradd --system --uid $UID --gid $GID --non-unique \
    --shell /bin/false \
    --home-dir /var/lib/transmission-daemon \
    debian-transmission

RUN echo Etc/UTC > /etc/timezone \
	&& echo 'APT::Install-Recommends "0";' \
		'APT::Install-Suggests "0";' \
		'APT::Acquire::Retries "20";' \
		'APT::Get::Assume-Yes "true";' \
		'Dpkg::Use-Pty "0";' \
		'quiet "1";' \
        >> /etc/apt/apt.conf.d/99gitlab

COPY test /

COPY cmd.sh /root/
COPY up.sh /root/
COPY route-pre-down.sh /root/
RUN chmod 0755 /root/cmd.sh /root/up.sh /root/route-pre-down.sh

RUN apt-get update \
 && apt-get install ca-certificates apt-transport-https \
 && sed -i 's,http://deb,https://deb,g' /etc/apt/sources.list \
 && apt-get update \
 && apt-get upgrade \
 && apt-get dist-upgrade \
 && apt-get install \
    cron \
    dumb-init \
    iptables \
    openvpn \
    sudo \
    transmission \
    transmission-cli \
    transmission-daemon \
    unattended-upgrades \
 && apt-get autoremove --purge \
 && apt-get clean \
 && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

#Transmission-RPC
EXPOSE 9091

CMD ["dumb-init", "/root/cmd.sh"]
