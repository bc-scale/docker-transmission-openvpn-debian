
A Docker image for running Transmission with OpenVPN based on the Debian
packages.  This is designed to be easy to setup, low maintenance and easily
auditable, even at the expense of configurability.

Inspired by https://github.com/haugene/docker-transmission-openvpn


## Usage



* TODO iptables rule to force all debian-transmission traffic over tun0


## Building

To build this to have a matching UID/GID as the host system:

    docker build \
		--build-arg UID=$(id -u debian-transmission) \
	    --build-arg GID=$(id -g debian-transmission) \
	    -t $(basename $(pwd)) .