#!/bin/bash -ex

# make sure we're running the latest things
apt-get update
apt-get dist-upgrade
apt-get autoremove --purge
apt-get clean

if [ ! -e /etc/openvpn/client/${VPNCONF}.conf ]; then
    echo ERROR: missing config file!
    exit 1
fi

# start cron for unattended-upgrades
/usr/sbin/cron -f &

# create the TUN device for OpenVPN
mkdir -p /dev/net
if [ ! -x /dev/net/tun ]; then
  mknod /dev/net/tun c 10 200
  chmod 0666 /dev/net/tun
fi

cd /etc/openvpn/client
/usr/sbin/openvpn \
    --verb 4 \
    --suppress-timestamps \
    --nobind \
    --config ${VPNCONF}.conf \
    --script-security 2 \
    --up-delay \
    --up /root/up.sh \
    --route-pre-down /root/route-pre-down.sh
